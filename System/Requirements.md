# Data logger

* [SYSREQ-1](#sysreq-1): Collect sensors data from 2 node devices wirelessly with maximum distance 2km then sends those data to control panel with query interval of 30 minutes for 2 nodes. In every 30 minutes, data logger will ensure that latest sensor data from 2 nodes appear properly on control panel.
* [SYSREQ-2](#sysreq-2): Able to control 2 relay connected to it by control panel
* [SYSREQ-3](#sysreq-3): Connected to Internet by WiFi configuration
* [SYSREQ-4](#sysreq-4): Only 1 widget on control panel to present the central physical device. This widget will indicate the online/offline status of data logger. 
* [SYSREQ-12](#sysreq-12): data logger is powered by adapter 12V

## SYSREQ-1

Downstream [SYSARCH-1](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md#data-logger), [SYSARCH-2](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md#data-logger), [SYSARCH-14](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md#data-logger), [SYSARCH-12](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md#data-logger), SYSARCH-16, SWARCH-1, SWARCH-2, SWARCH-19

## SYSREQ-2

Description:

1. Control [2 relays](https://hshop.vn/products/module-8-relay-voi-opto-coch-ly-koch-h-l-5vdc) connected to relay port in data logger by control panel. Every relay corresponds to 1 node.

2. On control panel, 1 control widget for 1 relay has 2 buttons: **RELAY** and **AUTOMATION MODE**:
    1. If **AUTOMATION MODE** = OFF, relay on central board can be controlled normally by **RELAY** button.
    2. If **AUTOMATION MODE** = ON:
        * Relay can't be controlled by **RELAY** button. 
        * If soil humidity of that node < humidity threshold value (setup in humidity threshold setup widget on dashboard), turn on that relay on central board and turn on the corresponded RELAY widget.
        * If soil humidity of that node > humidity threshold value (setup in humidity threshold setup widget on dashboard), turn off that relay on central board and turn off the corresponded RELAY widget

Downstream [SYSARCH-5](https://gitlab.com/TranPhucVinh/sis02/-/blob/main/System/Architecture.md#data-logger), [SWARCH-5](), [SWARCH-13]()

## SYSREQ-3

Description:

1. Have LED RGB to indicate WiFi status
2. Follow those steps to config WiFi:

**Step 1**: Press on config button for 5 seconds, **indicator LED turn green**

**Step 2**: A new access point, ``cau_hinh_wifi``, is generated. User can access to this AP successfully

**Step 3**: In ``cau_hinh_wifi``, go to ``192.168.4.1``. A configuration webpage will return.

**Step 4**: Enter SSID, password, control panel account credentials: useremail and password

Usecases:
1. Can't connect to WiFi by wrong SSID or password: **LED turns red**. User has to press config button again for LED to **turn green**.
2. Connect WiFi successfully (with valid SSID, password) and WiFi supports Internet, valid control panel credential (useremail and userpassword): **LED turns blue**
3. Connect WiFi successfully, WiFi doesn't support Internet, don't care control panel credential: **LED turns blue**
4. Connect WiFi successfully, WiFi supports internet, wrong control panel credential (useremail/userpassword): **LED turns blue**

# Node device

* [SYSREQ-5](#SYSREQ-5): Read environment temperature and humidity from a sensor and soil temperature and humidity from another sensor then sends those data wirelessly to data logger. 

**SYSREQ-13**: Node device is powered by lithium battery 124055 3000mAh 3.7V (file SIS02-ATT-LITHIUM_BATT_3.7V.jpg)

**SYSREQ-14**: Has power saving mode: Only measure sensor data when being queried data by data logger, enter sleep mode when not being queried. 

* [SYSREQ-15](#SYSREQ-15): 2 physical nodes are hard mapped to data logger. If a node has issue, admin must replace it by a new node, end user has nothing to deal with it.

**SYSREQ-16**: 2 physical nodes are presented on control panel by:
    * 1 online/offline status button
    * Relay control panel (SYSREQ-2)
    * 4 monitoring charts for each node (SYSREQ-11)

## SYSREQ-5

Downstream SYSARCH-8 SYSARCH-3 SYSARCH-4 SYSARCH-17

## SYSREQ-15

Implemented on control panel

**Downstream SYSARCH-10**

# Control panel

**SYSREQ-6**: Support only 1 customer user account, email stormnguyen1247@gmail.com (account with customer priviledge)	

**SYSREQ-7**: Control panel has map widget for the location of the deployed project

**SYSREQ-8**: Control panel has a widget to indicate the online/offline status of the physical node devices (2 nodes) and the data logger.

**SYSREQ-9**: Have 1 threshold setup widget for 1 node device for soil humidity threshold value. Allow user to enter value in this widget. So need totally 2 widgets for 2 nodes

**SYSREQ-10**: Have 1 alarm widget for 1 node for soil humidity threshold monitoring, so need totally 2 widgets for 2 nodes

For condition node soil humidity of that node < humidity threshold value (from ``SYSREQ-2``), trigger that alarm.

For condition node soil humidity of that node > humidity threshold value (from ``SYSREQ-2``), clear that alarm.

**SYSREQ-11**: 1 node has 1 sensor value table with 4 column:

* Soil humidity
* Soil temperature
* Environment humidity
* Environment temperature

## SYSREQ-8

Offline status:

* Data logger: If physical data logger is disconnected from ThingsBoard for 10 seconds, it will be offline 
* Node device: If a node device is disconnected from data logger for 30 minutes, it will be offline. (Query every 30 minutes for online status to save battery)

Online status: 

* Data logger: If central device is connected to ThingsBoard, which is indicated by blue light from LED, it will be online
* Node device: If a node sends data successfully to ThingsBoard (via data logger) and has its data upated on dashboard, it will be online. Expected time will be 30 minutes.
